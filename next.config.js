/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  baseUrl: process.env.NODE_ENV == 'production' ? '/acm-git' : '',
  assetPrefix: process.env.NODE_ENV == 'production' ? '/acm-git' : ''
}

module.exports = nextConfig
