import { GetStaticProps } from "next"
import path from "path"
import * as fs from "fs/promises";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";

type File = {
  filename: string,
  content: string
}

type HomeProps = {
  members: File[]
}

export default function Home({ members }: HomeProps) {
  return (
    <>
      <h1 className="text-2xl font-bold">Introducing the ACM members!</h1>
      {members.map(({ filename, content }) => (
          <div key={filename} className=" prose bg-white p-10 rounded-lg shadow-lg min-w-full">
            {<ReactMarkdown>{content}</ReactMarkdown>}
          </div>
      ))}
    </>
  )
}

export const getStaticProps: GetStaticProps<HomeProps> = async (ctx) => {
  const postsDirectory = path.join(process.cwd(), 'members')
  const filenames = await fs.readdir(postsDirectory)

  const members = filenames.map(async (filename) => {
    const filePath = path.join(postsDirectory, filename)
    const content = await fs.readFile(filePath, 'utf8')

    // Generally you would parse/transform the contents
    // For example you can transform markdown to HTML here

    return {
      filename,
      content,
    }
  })

  return {
    props: {
      members: await Promise.all(members)
    }
  }
}